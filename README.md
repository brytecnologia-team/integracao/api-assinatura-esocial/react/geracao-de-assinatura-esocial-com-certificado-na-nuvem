# Frontend ReactJS de Geração de Assinatura eSocial com certificado em nuvem.

Este exemplo apresenta os passos necessários para o frontend de geração de assinatura eSocial utilizando certificado digital instalado em nuvem, no BRy KMS.

### Backend

Este exemplo frontend é compatível com os backends:

* [Python](https://gitlab.com/brytecnologia-team/integracao/api-assinatura-esocial/python/backend-de-geracao-de-assinatura-esocial-com-certificado-na-nuvem-kms)
* [Java](https://gitlab.com/brytecnologia-team/integracao/api-assinatura-esocial/java/backend-de-geracao-de-assinatura-esocial-com-certificado-na-nuvem)


### Tech

O exemplo utiliza das bibliotecas JavaScript abaixo:
* [ReactJS] - A JavaScript library for building user interfaces!
* [Axios] - Promise based HTTP client for the browser and node.js.

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. 
Utilizamos o ReactJS versão 16.10 para desenvolvimento e o [Npm] versão 6.13 ou o [Yarn] versão 1.22 para instalação das dependências e execução da aplicação.

##### Comandos:

**Instalar as dependências utilizando o comando abaixo:**

    - npm install

  ou

    - yarn

**Executar programa:**

    - npm start

  ou

    - yarn start

   [Node]: <https://nodejs.org/en/>
   [ReactJS]: <https://reactjs.org/>
   [Axios]: <https://github.com/axios/axios>
   [Yarn]: <https://yarnpkg.com/>
   [Npm]: <https://www.npmjs.com/>
