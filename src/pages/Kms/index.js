import React, { useState } from "react";
import axios from 'axios';

export default function Kms() {

  // CRIA TODAS AS VARIAVEIS QUE SERÃO ENVIADAS NA REQUISIÇÃO
  const [documento, setDocumento] = useState("");
  const [loading, setLoading] = useState(false);
  const [uuid, setUuid] = useState("");


  // FUNCÃO QUE SERÁ EXECUTADA QUANDO FOR CLICADO NO BOTÃO "Assinar"
  async function handleSubmit(event) {

    // PREVINE AÇÕES PADÕRES DE UMA PAGINA REACT
    event.preventDefault();
    // ALTERA A VARIÁVEL LOADING PRA TRUE PARA QUE APAREÇA MENSAGEM DE "Realizando assinatura do documento"
    setLoading(true);
    // CRIA O FORMDATA QUE SERÁ ENVIADO NA REQUISIÇÃO
    const data = new FormData();
    // INCLUIR AS VARIÁVEIS NO FORMDATA
    // DOCUMENTO NO FORMATO XML QUE SERÁ ASSINADO
    data.append("documento", documento);
    //UUID do Certificado da assinatura
    data.append("uuid", uuid);

    try {
      // FAZ A REQUISIÇÃO DE ASSINATURA PARA O BACKEND
      const response = await axios.post("http://localhost:5000/assinarKMS", data);
      // FAZ DOWNLOAD DO ARQUIVO COM A HREF QUE VEM NA RESPOSTA DA REQUISIÇÃO
      console.log(response.data);
      window.location.href = response.data;
    } catch (err) {
      // CASO OCORRA ALGUM ERRO NA REQUISIÇÃO, MOSTRA UMA MENSAGEM PARA O USUÁRIO
      window.alert("Erro ao assinar o documento: " + err.response.data.message)
      console.log(err)
    }
    setLoading(false);
  }
  // HTML(JSX) DA PÁGINA  
  return (
    <>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <h2>Assinador BRy eSocial (KMS)</h2>
      {/* FORMULÁRIO PARA PEGAR OS DADOS DO USUÁRIO */}
      <form onSubmit={handleSubmit}>
        <label htmlFor="docLabel">Documento a ser assinado *</label>
        <label htmlFor="documento" className="fileUp">
          {documento ? (
            <React.Fragment>
              {documento.name}
            </React.Fragment>
          ) : (
              <React.Fragment>
                <i className="fa fa-upload"></i>
                Selecione o arquivo
              </React.Fragment>
            )}
          <input
            id="documento"
            type="file"
            accept=".xml"
            required
            onChange={event => setDocumento(event.target.files[0])}
          />
        </label>

        <label htmlFor="UUID">UUID *</label>
        <input
          id="uuid"
          type="string"
          required
          placeholder="UUID do certificado"
          value={uuid}
          onChange={event => setUuid(event.target.value)}
        />

        <button className="btn" type="submit">
          Assinar
        </button>

        <label>
          {loading ? "Realizando a assinatura do documento..." : ""}
        </label>
      </form>
    </>
  );
}
